package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

func handler(w http.ResponseWriter, req *http.Request) {
	Refresh()
	output := "Unknown API call"
	if strings.Contains(req.URL.Path, "/SmartPlugs/") && (strings.Contains(req.URL.Path, "Enable") || strings.Contains(req.URL.Path, "Disable")) {
		for _, v := range SmartPlugs {
			if strings.Contains(req.URL.Path, "Enable") && !v.IsEnabled() {
				go v.SetState(true)
			} else if strings.Contains(req.URL.Path, "Disable") && v.IsEnabled() {
				go v.SetState(false)
			}
		}
		if strings.Contains(req.URL.Path, "Enable") {
			output = "Enabled SmartPlugs"
		} else {
			output = "Disabled SmartPlugs"
		}
	} else {
		switch req.URL.Path {
		case "/SmartPlugs/Toggle":
			for _, v := range SmartPlugs {
				go log.Printf("Toggled smartplug %s to %t\n", v.Label, v.ToggleState())
			}
			if _, err := w.Write([]byte("Toggled smart plugs")); err != nil {
				log.Fatal("Could not reply to client")
			}
			output = "Toggled SmartPlugs"
			break
		case "/SmartPlugs/GetState":
			if data, err := json.Marshal(SmartPlugs); err == nil {
				output = string(data)
			} else {
				log.Fatal(err)
			}
			break
		case "/Sensors/Temperature":
			output = ""
			for _, v := range Temperatures {
				output += fmt.Sprintf("Temperature in %s is currently %sc", v.Label, v.Temperature)
			}
			go GetTemperatures()
			break
		}
	}
	if _, err := w.Write([]byte(output)); err != nil {
		log.Fatal("Could not reply to client")
	}
}

func startServer(ip string) {
	log.Printf("Starting http server on port 8085 bound to %s\n", ip)
	server := &http.Server{
		Addr:           fmt.Sprintf("%s:8085", ip),
		Handler:        http.HandlerFunc(handler),
		ReadTimeout:    20 * time.Second,
		WriteTimeout:   20 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	log.Fatal(server.ListenAndServe())
}
