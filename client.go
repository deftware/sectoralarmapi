package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	API = "mypagesapi.sectoralarm.net"
	CurrentSession *Session
)

type Session struct {
	LastLogin int64
	Active bool
	Version string
	RawVersion string
	CookieJar *cookiejar.Jar
	Client *http.Client
}

func (s *Session) postRequest(endpoint, payload string) (string, error) {
	req, err := http.NewRequest("POST", fmt.Sprintf("https://%s/%s", API, endpoint), bytes.NewBuffer([]byte(payload)))
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json, text/plain, */*")
	res, err := s.Client.Do(req)
	if err != nil {
		return "", err
	}
	data, err := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	return string(data), err
}

func (s *Session) getRequest(endpoint string) (string, error) {
	res, err := s.Client.Get(fmt.Sprintf("https://%s/%s", API, endpoint))
	if err != nil {
		log.Fatal(err)
	}
	data, err := ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	return string(data), err
}

func createSession() *Session {
	cookieJar, _ := cookiejar.New(nil)
	CurrentSession = &Session{
		Active:  false,
		CookieJar: cookieJar,
		Client: &http.Client{
			Jar: cookieJar,
		},
	}
	if len(os.Getenv("SA_VERSION")) != 0 {
		CurrentSession.RawVersion = os.Getenv("SA_VERSION")
	} else {
		data, _ := CurrentSession.getRequest("/User/Login")
		regex := regexp.MustCompile("v[0-9]_[0-9]_[0-9]+")
		matches := regex.FindAllString(data, -1)
		CurrentSession.RawVersion = matches[0]
	}
	CurrentSession.Version = strings.ReplaceAll(CurrentSession.RawVersion, "_", ".")
	log.Printf("Connected to Sector Alarm %s\n", CurrentSession.Version)
	return CurrentSession
}

func Refresh() {
	if CurrentSession.LastLogin + (30 * 60 * 1000) < time.Now().UnixNano() / int64(time.Millisecond) {
		log.Println("Refreshing session")
		Login()
		go GetTemperatures()
	}
}

func Login() *Session {
	createSession()
	form := url.Values{}
	form.Add("userID", os.Getenv("SA_EMAIL"))
	form.Add("password", os.Getenv("SA_PASSWORD"))
	res, err := CurrentSession.Client.PostForm(fmt.Sprintf("https://%s/%s", API, "/User/Login?ReturnUrl=%2f"), form)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	_, err = ioutil.ReadAll(res.Body)
	_ = res.Body.Close()
	if res.StatusCode == 200 {
		CurrentSession.LastLogin = time.Now().UnixNano() / int64(time.Millisecond)
		log.Println("Successfully logged in with provided email and password")
	}
	return CurrentSession
}
