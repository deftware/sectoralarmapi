# Sector Alarm (Unofficial) API

Simple Sector Alarm API written in go for managing your alarm system with ease.

# Features

* Reading temperature sensors
* Toggling SmartPlugs
* Reading alarm state history
* Toggling the alarm

# Install/Usage

1. [Download](https://gitlab.com/deftware/sectoralarmapi/-/jobs/517486602/artifacts/browse/build/) the correct binary for your server
2. On Linux/macOS, you need to make it executable (`$ sudo chmod +x /path/to/binary`) and allow it to run without sudo `$ sudo setcap CAP_NET_BIND_SERVICE=+eip /path/to/binary`
3. Set environment variables prior to executing the binary, i.e. these `SA_EMAIL, SA_PASSWORD, SA_SITE_ID`, on Linux you can use one line to set them and execute the binary: `env SA_EMAIL=<your email> env SA_PASSWORD=<your password> env SA_SITE_ID=<your site id> /path/to/binary --http "0.0.0.0"`
4. The server will now run and listen on port `8085` for commands

# Homebridge support

This server can be used with my [homebridge-sectorswitch](https://www.npmjs.com/package/homebridge-sectorswitch) plugin

# License

This is licensed under the MIT license.
