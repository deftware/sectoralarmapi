package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

const (
	FullArm = "Total"
	PartialArm = "Partial"
	Disarm = "Disarm"
)

type ArmResponse struct {
	Status string `json:"status"`
	Message string `json:"message"`
	Time string `json:"time"`
	User string `json:"user"`
	PanelData *PanelData `json:"panelData"`
	ReloadLocks bool `json:"ReloadLocks"`
}

func Arm(mode string) ArmResponse {
	payload := fmt.Sprintf(`{"ArmCmd": "%s", "PanelCode": "%s", "HasLocks": false, "id": "%s"}`, mode, os.Getenv("SA_PANEL_CODE"), os.Getenv("SA_SITE_ID"))
	data, err := CurrentSession.postRequest("/Panel/ArmPanel/", payload)
	if err != nil {
		log.Fatal(err)
	}
	var structData ArmResponse
	_ = json.Unmarshal([]byte(data), &structData)
	return structData
}
